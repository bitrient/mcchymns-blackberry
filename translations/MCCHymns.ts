<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>About</name>
    <message>
        <location filename="../assets/About.qml" line="6"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/About.qml" line="49"/>
        <source>MCCHymns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/About.qml" line="58"/>
        <source>Sacred songs and solos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/About.qml" line="69"/>
        <source>Twelve hundred hymns
as compiled under the direction of</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/About.qml" line="78"/>
        <source>IRA D. SANKEY</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/About.qml" line="101"/>
        <source>Bitrient Inc.    2015</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../assets/Settings.qml" line="17"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="30"/>
        <source>Settings scrollView</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="49"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="54"/>
        <source>Show favorites on start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="72"/>
        <source>Hymn settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="368"/>
        <source>Background Texture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Settings.qml" line="373"/>
        <source>Use textured background</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>favoritesList</name>
    <message>
        <location filename="../assets/favoritesList.qml" line="28"/>
        <source>List of all the hymns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="163"/>
        <source>No hymn found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="170"/>
        <source>Add hymn to Favorites and it will show up here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="180"/>
        <source>Open hymn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="190"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="200"/>
        <source>Sort By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="210"/>
        <source>Remove All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/favoritesList.qml" line="282"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>hymnView</name>
    <message>
        <location filename="../assets/hymnView.qml" line="30"/>
        <source>Double tap to hide or show chorus pane.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnView.qml" line="32"/>
        <source>Got It</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnView.qml" line="99"/>
        <source> Add to Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnView.qml" line="123"/>
        <source>Hymn scrollview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnView.qml" line="160"/>
        <source>Chorus</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>hymnsList</name>
    <message>
        <location filename="../assets/hymnsList.qml" line="29"/>
        <source>List of all the hymns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnsList.qml" line="113"/>
        <source>Loading hymns ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnsList.qml" line="123"/>
        <source>Open hymn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnsList.qml" line="133"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnsList.qml" line="142"/>
        <source>Sort By</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/hymnsList.qml" line="204"/>
        <source>MCCHymns</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="94"/>
        <source>MCCHymns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="106"/>
        <source>Favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="32"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="49"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
