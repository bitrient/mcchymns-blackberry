import bb.cascades 1.4

Container {
    id: divider
    
    background: Color.create(appSettings.fontColor)
    preferredHeight: ui.sdu(0.2)
    preferredWidth: ui.du(200)
}
